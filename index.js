require("dotenv").config();
const express = require("express");
const cors = require("cors");
const app = express();

const dbConnection = require("./dbConnection");

// On DB connection read the current index to continue from there
dbConnection.on("connected", async () => {
  const Url = require("./models");
  try {
    const count = await Url.count();
    app.set("counter", count);
  } catch (err) {
    console.log(err);
  }
});

// Basic Configuration
const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use("/public", express.static(`${process.cwd()}/public`));

app.get("/", function (req, res) {
  res.sendFile(process.cwd() + "/views/index.html");
});

// add the api routes
app.use(require("./api"));

app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});
