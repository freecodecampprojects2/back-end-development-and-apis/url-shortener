require("dotenv").config();
const mongoose = require("mongoose");

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on("error", (err) => {
  console.error(err);
});

mongoose.connection.on("connecting", (_) => {
  console.log("MongoDB connecting!");
});

/**
mongoose.connection.on("connected", (_) => {
  console.log("MongoDB connected!");
});
*/

/** 
mongoose.connection.on("open", () => {
  console.log("MongoDB connection open!");
});
*/

mongoose.connection.on("disconnecting", (_) => {
  console.log("MongoDB disconnecting!");
});

mongoose.connection.on("disconnected", (_) => {
  console.log("MongoDB disconnected!");
});

mongoose.connection.on("close", (_) => {
  console.log("MongoDB closed!");
});

module.exports = mongoose.connection;
