const Url = require("../models");
async function getUrl(original_url) {
  try {
    const result = await Url.findOne(
      { original_url },
      "original_url short_url"
    );
    return result;
  } catch (err) {
    throw err;
  }
}

async function getShortUrl(sh_ur) {
  try {
    const { original_url, short_url } = await Url.findOne({ short_url: sh_ur });

    return { original_url, short_url };
  } catch (err) {
    throw err;
  }
}

async function createNewUrl(original, short) {
  try {
    const { original_url, short_url } = await Url.create({
      original_url: original,
      short_url: short,
    });
    return { original_url, short_url };
  } catch (err) {
    throw err;
  }
}

module.exports = { getUrl, createNewUrl, getShortUrl };
