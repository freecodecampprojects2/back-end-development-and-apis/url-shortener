const { Router } = require("express");
const { newUrlMiddleware, validityMiddleware } = require("./middleware");
const { getShortUrl } = require("../services");

const router = new Router();

router.post(
  "/api/shorturl",
  validityMiddleware,
  newUrlMiddleware,
  (req, res) => {
    res.json(req.URL);
  }
);

router.get("/api/shorturl/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const { original_url } = await getShortUrl(id);
    res.status(301).redirect(original_url);
  } catch (err) {
    res.status(400).json(err);
  }
});

module.exports = router;
