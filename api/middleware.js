const error = "Invalid URL";
const { URL } = require("url");
const dns = require("dns");
const { getUrl, createNewUrl } = require("../services");

async function validityMiddleware(req, res, next) {
  const { url } = req.body;
  try {
    const validUrl = new URL(url);
    dns.lookup(validUrl.host, (err) => {
      if (err) throw err;
      req.validUrl = validUrl.href;
      next();
    });
  } catch (err) {
    res.status(400).json({ error });
  }
}

async function newUrlMiddleware(req, res, next) {
  const { validUrl } = req;
  const counter = req.app.get("counter");
  try {
    const exists = await getUrl(validUrl);
    if (exists) {
      const { original_url, short_url } = exists;
      req.URL = { original_url, short_url };
      next();
    } else {
      const newUrl = await createNewUrl(validUrl, counter);
      req.app.set("counter", counter + 1);
      req.URL = newUrl;
      next();
    }
  } catch (err) {
    res.status(400).json({ error });
  }
}

module.exports = { validityMiddleware, newUrlMiddleware };
